from datetime import date, datetime, timezone
from typing import List, Optional, ClassVar, Dict

from pydantic import BaseModel, RootModel, validator


def convert_datetime_to_iso_8601(dt: datetime) -> str:
    return dt.isoformat(timespec="milliseconds")


def convert_date_str(dt: datetime.date) -> str:
    return dt.strftime("%Y-%m-%d")


class ExternalId(BaseModel):
    name: Optional[str] = None
    value: Optional[str] = None


class CustomFields(BaseModel):
    name: Optional[str] = None
    value: Optional[str] = None


class AssociatedAccount(BaseModel):
    accountId: Optional[str] = None
    accountNumber: Optional[str] = None
    accountName: Optional[str] = None


class Account(BaseModel):
    schema_name: ClassVar[str] = "Accounts"
    id: Optional[str] = None
    name: Optional[str] = None
    accountNumber: Optional[str] = None
    parentId: Optional[str] = None
    parentName: Optional[str] = None
    type: Optional[str] = None
    description: Optional[str] = None
    category: Optional[str] = None
    currency: Optional[str] = None
    active: Optional[bool] = None
    subsidiary: Optional[str] = None
    subsidiaryId: Optional[str] = None
    isBankAccount: Optional[bool] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    hg_deleted: Optional[bool] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Accounts(RootModel):
    root: List[Account]


class GeneralLedgerReport(BaseModel):
    schema_name: ClassVar[str] = "GeneralLedgerReport"
    classId: Optional[str] = None
    className: Optional[str] = None
    accountId: Optional[str] = None
    accountName: Optional[str] = None
    accountNumber: Optional[str] = None
    departmentId: Optional[str] = None
    departmentName: Optional[str] = None
    departmentCode: Optional[str] = None
    nameId: Optional[str] = None
    name: Optional[str] = None
    customerId: Optional[str] = None
    customerName: Optional[str] = None
    vendorId: Optional[str] = None
    vendorName: Optional[str] = None
    amount: Optional[float] = None
    categories: Optional[str] = None
    location: Optional[str] = None
    locationId: Optional[str] = None
    subsidiary: Optional[str] = None
    subsidiaryId: Optional[str] = None
    project: Optional[str] = None
    projectId: Optional[str] = None
    currency: Optional[str] = None
    id: Optional[str] = None
    documentType: Optional[str] = None
    documentNumber: Optional[str] = None
    postingDate: Optional[datetime] = None
    postingPeriod: Optional[str] = None
    accountType: Optional[str] = None
    transactionDescription: Optional[str] = None
    transactionId: Optional[str] = None
    fiscalStartDate: Optional[str] = None
    fiscalPeriod: Optional[str] = None


class GeneralLedgerReports(RootModel):
    root: List[GeneralLedgerReport]


class BankAccount(BaseModel):
    schema_name: ClassVar[str] = "Bank Accounts"
    id: Optional[str] = None
    name: Optional[str] = None
    accountNumber: Optional[str] = None
    type: Optional[str] = None
    description: Optional[str] = None
    currency: Optional[str] = None
    active: Optional[bool] = None
    holderName: Optional[str] = None
    swiftCode: Optional[str] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    hg_deleted: Optional[bool] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class BankAccounts(RootModel):
    root: List[BankAccount]


class PhoneNumber(BaseModel):
    type: Optional[str] = None
    number: Optional[str] = None


class Address(BaseModel):
    id: Optional[str] = None
    addressType: Optional[str] = None
    addressText: Optional[str] = None
    line1: Optional[str] = None
    line2: Optional[str] = None
    line3: Optional[str] = None
    city: Optional[str] = None
    state: Optional[str] = None
    postalCode: Optional[str] = None
    country: Optional[str] = None


class Reference(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None


class Customer(BaseModel):
    schema_name: ClassVar[str] = "Customers"
    id: Optional[str] = None
    customerName: Optional[str] = None
    contactName: Optional[str] = None
    emailAddress: Optional[str] = None
    firstName: Optional[str] = None
    lastName: Optional[str] = None
    middleName: Optional[str] = None
    suffix: Optional[str] = None
    title: Optional[str] = None
    website: Optional[str] = None
    phoneNumbers: Optional[List[PhoneNumber]] = None
    addresses: Optional[List[Address]] = None
    notes: Optional[str] = None
    checkName: Optional[str] = None
    parentReference: Optional[Reference] = None
    paymentMethod: Optional[str] = None
    balance: Optional[float] = None
    balanceDate: Optional[datetime] = None
    customerType: Optional[str] = None
    salesTerm: Optional[str] = None
    taxable: Optional[bool] = None
    taxCode: Optional[str] = None
    active: Optional[bool] = None
    ownerId: Optional[str] = None
    currency: Optional[str] = None
    subsidiary: Optional[str] = None
    customFields: Optional[List[CustomFields]] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Customers(RootModel):
    root: List[Customer]


class Vendor(BaseModel):
    schema_name: ClassVar[str] = "Vendors"
    id: Optional[str] = None
    vendorNumber: Optional[str] = None
    vendorName: Optional[str] = None
    contactName: Optional[str] = None
    emailAddress: Optional[str] = None
    phoneNumbers: Optional[List[PhoneNumber]] = None
    addresses: Optional[List[Address]] = None
    currency: Optional[str] = None
    active: Optional[bool] = None
    vendorCode: Optional[str] = None
    vendorGroupCode: Optional[str] = None
    taxPayerNumber: Optional[str] = None
    paymentTerm: Optional[str] = None
    bankAccounts: Optional[List[BankAccount]] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    hg_deleted: Optional[bool] = None
    tenant_id: Optional[str] = None
    isPerson: Optional[bool] = None
    category: Optional[str] = None


    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Vendors(RootModel):
    root: List[Vendor]


class Project(BaseModel):
    schema_name: ClassVar[str] = "Projects"
    id: Optional[str] = None
    projectNumber: Optional[str] = None
    name: Optional[str] = None
    description: Optional[str] = None
    currency: Optional[str] = None
    category: Optional[str] = None
    active: Optional[bool] = None
    startDate: Optional[datetime] = None
    endDate: Optional[datetime] = None
    customerName: Optional[str] = None
    customerId: Optional[str] = None
    classId: Optional[str] = None
    type: Optional[str] = None
    departmentId: Optional[str] = None
    departmentName: Optional[str] = None
    location: Optional[str] = None
    locationId: Optional[str] = None
    locationName: Optional[str] = None
    className: Optional[str] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    customFields: Optional[List[CustomFields]] = None
    hg_deleted: Optional[bool] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Projects(RootModel):
    root: List[Project]


class BillInvoiceItem(BaseModel):
    description: Optional[str] = None
    unitPrice: Optional[float] = None
    accountId: Optional[str] = None


class Item(BaseModel):
    schema_name: ClassVar[str] = "Items"
    id: Optional[str] = None
    name: Optional[str] = None
    displayName: Optional[str] = None
    accounts: Optional[Dict[str, AssociatedAccount]] = None
    sku: Optional[str] = None
    reorderPoint: Optional[str] = None
    quantityOnHand: Optional[float] = None
    taxable: Optional[bool] = None
    taxCode: Optional[str] = None
    invStartDate: Optional[datetime] = None
    code: Optional[str] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    active: Optional[bool] = None
    type: Optional[str] = None
    category: Optional[str] = None
    isBillItem: Optional[bool] = None
    billItem: Optional[BillInvoiceItem] = None
    isInvoiceItem: Optional[bool] = None
    invoiceItem: Optional[BillInvoiceItem] = None
    customFields: Optional[List[CustomFields]] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Items(RootModel):
    root: List[Item]


class InvoiceLineItem(BaseModel):
    id: Optional[str] = None
    productId: Optional[str] = None
    productName: Optional[str] = None
    accountName: Optional[str] = None
    accountNumber: Optional[str] = None
    description: Optional[str] = None
    taxCode: Optional[str] = None
    discount: Optional[float] = None
    quantity: Optional[float] = None
    unitPrice: Optional[float] = None
    totalPrice: Optional[float] = None
    serviceDate: Optional[datetime] = None
    customFields: Optional[List[CustomFields]] = None
    classId: Optional[str] = None
    className: Optional[str] = None


class BillLineItem(BaseModel):
    id: Optional[str] = None
    orderId: Optional[str] = None
    productId: Optional[str] = None
    productName: Optional[str] = None
    accountName: Optional[str] = None
    accountNumber: Optional[str] = None
    description: Optional[str] = None
    taxCode: Optional[str] = None
    discount: Optional[float] = None
    quantity: Optional[float] = None
    unitPrice: Optional[float] = None
    totalPrice: Optional[float] = None
    serviceDate: Optional[datetime] = None
    customFields: Optional[List[CustomFields]] = None
    classId: Optional[str] = None
    className: Optional[str] = None


class Expense(BaseModel):
    id: Optional[str] = None
    accountNumber: Optional[str] = None
    accountId: Optional[str] = None
    amount: Optional[float] = None
    department: Optional[str] = None
    taxAmount: Optional[float] = None
    taxCode: Optional[str] = None


class Invoice(BaseModel):
    schema_name: ClassVar[str] = "Invoices"
    id: Optional[str] = None
    invoiceNumber: Optional[str] = None
    billEmail: Optional[str] = None
    lineItems: Optional[List[InvoiceLineItem]] = None
    salesTerm: Optional[str] = None
    customerName: Optional[str] = None
    customerMemo: Optional[str] = None
    customerId: Optional[str] = None
    currency: Optional[str] = None
    issueDate: Optional[datetime] = None
    dueDate: Optional[datetime] = None
    shipMethod: Optional[str] = None
    shipDate: Optional[datetime] = None
    trackingNumber: Optional[str] = None
    invoiceMemo: Optional[str] = None
    paymentMethod: Optional[str] = None
    paidDate: Optional[datetime] = None
    address: Optional[List[Address]] = None
    subTotal: Optional[float] = None
    deposit: Optional[float] = None
    totalAmount: Optional[float] = None
    totalDiscount: Optional[float] = None
    amountDue: Optional[float] = None
    taxCode: Optional[str] = None
    status: Optional[str] = None
    subsidiary: Optional[str] = None
    createdAt: Optional[datetime] = None
    updatedAt: Optional[datetime] = None
    customFields: Optional[List[CustomFields]] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Invoices(RootModel):
    root: List[Invoice]


class Attachment(BaseModel):
    schema_name: ClassVar[str] = "Attachments"
    id: Optional[str] = None
    name: Optional[str] = None
    url: Optional[str] = None


class Bill(BaseModel):
    schema_name: ClassVar[str] = "Bills"
    id: Optional[str] = None
    number: Optional[str] = None
    externalId: Optional[ExternalId] = None
    lineItems: Optional[List[BillLineItem]] = None
    expenses: Optional[List[Expense]] = None
    vendorName: Optional[str] = None
    vendorId: Optional[str] = None
    currency: Optional[str] = None
    accountName: Optional[str] = None
    accountCode: Optional[str] = None
    notes: Optional[str] = None
    status: Optional[str] = None
    paidDate: Optional[datetime] = None
    dueDate: Optional[datetime] = None
    balance: Optional[float] = None
    location: Optional[str] = None
    locationId: Optional[str] = None
    currencyBalance: Optional[float] = None
    totalAmount: Optional[float] = None
    attachments: Optional[List[Attachment]] = None
    createdAt: Optional[datetime] = None
    updatedAt: Optional[datetime] = None
    issueDate: Optional[datetime] = None
    hg_deleted: Optional[bool] = None
    customFields: Optional[List[CustomFields]] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Bills(RootModel):
    root: List[Bill]


class JournalLine(BaseModel):
    accountId: Optional[str] = None
    accountNumber: Optional[str] = None
    accountName: Optional[str] = None
    locationId: Optional[str] = None
    locationName: Optional[str] = None
    className: Optional[str] = None
    classId: Optional[str] = None
    departmentName: Optional[str] = None
    departmentId: Optional[str] = None
    customerName: Optional[str] = None
    description: Optional[str] = None
    amount: Optional[float] = None
    postingType: Optional[str] = None
    costCenter: Optional[str] = None
    costUnit: Optional[str] = None
    projectId: Optional[str] = None
    projectName: Optional[str] = None
    taxCode: Optional[str] = None
    customFields: Optional[List[CustomFields]] = None


class JournalEntry(BaseModel):
    schema_name: ClassVar[str] = "JournalEntries"
    id: Optional[str] = None
    className: Optional[str] = None
    classId: Optional[str] = None
    departmentName: Optional[str] = None
    departmentId: Optional[str] = None
    transactionDate: Optional[datetime] = None
    currency: Optional[str] = None
    journalLines: Optional[List[JournalLine]] = None
    supplierName: Optional[str] = None
    description: Optional[str] = None
    paymentReference: Optional[str] = None
    number: Optional[str] = None
    tenant_id: Optional[str] = None
    customFields: Optional[List[CustomFields]] = None

    @validator("transactionDate")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class JournalEntries(RootModel):
    root: List[JournalEntry]


class Class(BaseModel):
    schema_name: ClassVar[str] = "Classes"
    id: Optional[str] = None
    classNumber: Optional[str] = None
    name: Optional[str] = None
    description: Optional[str] = None
    active: Optional[bool] = None
    parentId: Optional[str] = None
    parentName: Optional[str] = None
    subsidiary: Optional[str] = None
    subsidiaryId: Optional[str] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    hg_deleted: Optional[bool] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Classes(RootModel):
    root: List[Class]


class CustomerType(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    active: Optional[bool] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class CustomerTypes(RootModel):
    root: List[CustomerType]


class PaymentMethod(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    paymentType: Optional[str] = None
    active: Optional[bool] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class PaymentMethods(RootModel):
    root: List[PaymentMethod]


class PaymentTerm(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    paymentType: Optional[str] = None
    discountPercent: Optional[float] = None
    discountDays: Optional[float] = None
    active: Optional[bool] = None
    dayOfMonthDue: Optional[int] = None
    dueNextMonthDays: Optional[float] = None
    dueDays: Optional[float] = None
    discountDayOfMonth: Optional[int] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class PaymentTerms(RootModel):
    root: List[PaymentTerm]


class Department(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    active: Optional[bool] = None
    subDepartment: Optional[bool] = None
    code: Optional[str] = None
    parentId: Optional[str] = None
    parentName: Optional[str] = None
    subsidiary: Optional[str] = None
    subsidiaryId: Optional[str] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    hg_deleted: Optional[bool] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Departments(RootModel):
    root: List[Department]


class TaxRate(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    active: Optional[bool] = None
    rateValue: Optional[float] = None
    specialTaxType: Optional[str] = None
    agencyId: Optional[int] = None
    effectiveTaxRate: Optional[float] = None
    description: Optional[str] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class TaxRates(RootModel):
    root: List[TaxRate]


class TaxCode(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    taxGroup: Optional[bool] = None
    taxable: Optional[bool] = None
    active: Optional[bool] = None
    description: Optional[str] = None
    hidden: Optional[bool] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class TaxCodes(RootModel):
    root: List[TaxCode]


class APAdjustmentLine(BaseModel):
    accountId: Optional[str] = None
    accountNumber: Optional[str] = None
    accountName: Optional[str] = None
    locationId: Optional[str] = None
    locationName: Optional[str] = None
    amount: Optional[float] = None
    memo: Optional[str] = None
    vendorId: Optional[str] = None
    vendorName: Optional[str] = None
    projectId: Optional[str] = None
    projectName: Optional[str] = None
    classId: Optional[str] = None
    className: Optional[str] = None
    departmentId: Optional[str] = None
    departmentName: Optional[str] = None
    status: Optional[str] = None


class AccountPayableAdjustment(BaseModel):
    schema_name: ClassVar[str] = "APAdjustment"
    id: Optional[str] = None
    vendorId: Optional[str] = None
    vendorName: Optional[str] = None
    billNumber: Optional[str] = None
    adjustmentNumber: Optional[str] = None
    description: Optional[str] = None
    currency: Optional[str] = None
    transactionDate: Optional[datetime] = None
    lineItems: Optional[List[APAdjustmentLine]] = None


    @validator("transactionDate")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class PnLDetailReportLine(BaseModel):
    id: Optional[str] = None
    number: Optional[str] = None
    transactionDate: Optional[date] = None
    transactionType: Optional[str] = None
    createdAt: Optional[datetime] = None
    updatedAt: Optional[datetime] = None
    description: Optional[str] = None
    entityName: Optional[str] = None
    entityId: Optional[str] = None
    className: Optional[str] = None
    classId: Optional[str] = None
    locationName: Optional[str] = None
    locationId: Optional[str] = None
    accountId: Optional[str] = None
    accountName: Optional[str] = None
    amount: Optional[float] = None
    categories: Optional[List[str]] = None
    departmentName: Optional[str] = None
    departmentId: Optional[str] = None
    companyName: Optional[str] = None
    taxName: Optional[str] = None
    taxAmount: Optional[float] = None
    tenant_id: Optional[str] = None

    class Schema:
        schema_name = "PnLDetailReportLine"


class PnLDetailReport(RootModel):
    root: List[PnLDetailReportLine]

class Location(BaseModel):
    schema_name: ClassVar[str] = "Locations"
    id: Optional[str] = None
    name: Optional[str] = None
    locationNumber: Optional[str] = None
    parentId: Optional[str] = None
    active: Optional[bool] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    locationType: Optional[str] = None
    currency: Optional[str] = None
    hg_deleted: Optional[bool] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}

class Locations(RootModel):
    root: List[Location]

class Task(BaseModel):
    schema_name: ClassVar[str] = "Tasks"
    id: Optional[str] = None
    taskNumber: Optional[str] = None
    name: Optional[str] = None
    description: Optional[str] = None
    projectId: Optional[str] = None
    projectName: Optional[str] = None
    active: Optional[bool] = None
    priority: Optional[str] = None
    startDate: Optional[datetime] = None
    endDate: Optional[datetime] = None
    assigneeId: Optional[str] = None
    billable: Optional[bool] = None
    rateCurrency: Optional[str] = None
    rateValue: Optional[float] = None
    timeSpentHours: Optional[int] = None
    timeSpentMinutes: Optional[int] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    hg_deleted: Optional[bool] = None

    @validator("updatedAt", "createdAt", "startDate", "endDate")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}

class Tasks(RootModel):
    root: List[Task]

class TransactionPayment(BaseModel):
    schema_name: ClassVar[str] = "TransactionPayment"
    id: Optional[str] = None
    transaction_id: Optional[str] = None
    amount: Optional[float] = None

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}

class CreditMemo(BaseModel):
    schema_name: ClassVar[str] = "CreditMemo"
    id: Optional[str] = None
    note: Optional[str] = None
    status: Optional[str] = None
    currency: Optional[str] = None
    subTotal: Optional[float] = None
    issueDate: Optional[datetime] = None
    lineItems: Optional[str] = None
    customerId: Optional[str] = None
    customerName: Optional[str] = None
    totalAmount: Optional[float] = None
    currencyRate: Optional[str] = None
    modifiedDate: Optional[datetime] = None
    totalDiscount: Optional[float] = None
    totalTaxAmount: Optional[float] = None
    allocatedOnDate: Optional[datetime] = None
    remainingCredit: Optional[float] = None
    creditNoteNumber: Optional[str] = None
    discountPercentage: Optional[float] = None
    paymentAllocations: Optional[str] = None
    location: Optional[str] = None
    locationId: Optional[str] = None
    subsidiary: Optional[str] = None
    subsidiaryId: Optional[str] = None

    @validator("modifiedDate", "allocatedOnDate", "issueDate")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Subsidiary(BaseModel):
    schema_name: ClassVar[str] = "Subsidiaries"
    id: Optional[str] = None
    externalId: Optional[str] = None
    name: Optional[str] = None
    fullname: Optional[str] = None
    addresses: Optional[List[Address]] = None
    email: Optional[str] = None
    state: Optional[str] = None
    website: Optional[str] = None
    parentId: Optional[str] = None
    parentName: Optional[str] = None
    currencyId: Optional[str] = None
    currency: Optional[str] = None
    currencyName: Optional[str] = None
    active: Optional[bool] = None
    updatedAt: Optional[datetime] = None
    createdAt: Optional[datetime] = None
    tenant_id: Optional[str] = None

    @validator("updatedAt", "createdAt")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class Subsidiaries(RootModel):
    root: List[Subsidiary]