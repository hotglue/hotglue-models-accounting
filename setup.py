#!/usr/bin/env python

from setuptools import setup

setup(
    name="hotglue-models-accounting",
    version="0.0.11",
    description="hotglue model definition for Accounting",
    author="hotglue",
    url="https://hotglue.xyz",
    classifiers=["Programming Language :: Python :: 3 :: Only"],
    install_requires=["pydantic>=2.5.3", "typing_extensions>=4.6.1", "simplejson"],
    packages=["hotglue_models_accounting"],
)
